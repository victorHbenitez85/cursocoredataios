//
//  ViewController.swift
//  myAppContacto
//
//  Created by Victor Hugo Benitez Bosques on 31/08/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
// Checar : App Programming Guide for IOS

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource{

    @IBOutlet var tableView: UITableView!
    
    var people = [NSManagedObject]() //Guarda instancias de Manage Model Object : De la entidad Person
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Lista de Contactos" //nombre de la vista
        
        // asiganmos que tableView gestine las celdas de nuestra tabla
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell") // UITableViewCell.self : Mostrara celdas por defecto, forCellReuseIdentifier : identificador de la celdas de la UItableView CREACION DE UNA UITableView CODIGO
       
    }
    
    override func viewWillAppear(animated: Bool) { //funcion que se ejecuta a punto de cargar la vista de la app
        super.viewWillAppear(animated)
        
        // Gestionar entradas y salidas de Core Data
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate    //necesitamos recuperar el delegado para usar el metodo Manage Object Context
        let managedContext = appDelegate.managedObjectContext           // el metodo para realizar la persistencia en disco
        
        //peticion para traer los objetos de la entidad a Core Data
        let fetchRequest = NSFetchRequest(entityName: "Person") //Peticion para consulta de los objetos
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest) //devolvera el array de objetos tipo personas NSObject
            people = results as! [NSManagedObject]  //asignamos el array de objetos a el array people usando un casting de NSMaganedObject
            
        }catch let error as NSError{
            print("No hemos podido recuperar los  datos debido al erro \(error) \(error.userInfo)") //userInfo array de los estados de las variables
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func addName(sender: UIBarButtonItem) { // figura 1.- Se mostrar un alertAction que tendra un textfiel y bottones de agregar o cancelar nombres de Contactos
        
        let alertController = UIAlertController(title: "Nuevo Contacto", message: "Introduce el nombre del contacto", preferredStyle: .Alert) //Creacion de la alerta tendra los botones y el textField .Alert Coloca la alerta centro de la pantalla
        let saveAction = UIAlertAction(title: "Guardar", style: .Default) { (action) in  //boton de Guardar handler -> accion despues de crear el boton se mostrara el textfield
            let textField = alertController.textFields?.first   // Crear una textfield
            self.savePersonWithName(textField!.text!)           // el texto del textfield no se puede guardar de manera directa por el Manage model object
            self.tableView.reloadData()                         // Necesario para que muestre en las celdas los nombres introducidos : Recargamos tableView
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil) //Creacion del boton cancelar
        
        alertController.addTextFieldWithConfigurationHandler(nil)  // Agregamos el textField configurado en saveAction
        alertController.addAction(saveAction)                      // Agregado el boton saveAction al AlertController
        alertController.addAction(cancelAction)                    // Agregado el boton cacelAction alertController
        
        self.presentViewController(alertController, animated: true, completion: nil) // Mostramos en la vista el alerController
        
    }
    
    
    /* array people guarda objetos de tipo ManageModelObjet se necesita tratar para guardar el texto del textField*/
    
    func savePersonWithName(name: String){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate    // 1. Recuperar el delegado para ocupar los metodos asignados para Core Data usamos el metodo managedObjectContext
        let managedContext = appDelegate.managedObjectContext                           // 1.2 Se usa para poder guardar en disco tarea de persistencia : traido del appDelegate
        
        let entity = NSEntityDescription.entityForName("Person", inManagedObjectContext: managedContext) //2.0Indicamos la entidad donde se guardar el objeto creado parametros: nameEntidad, quien guarda la entidad:managedContext
        let person  = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext) //2.1 Creamos el objeto Managed Object
        person.setValue(name, forKey: "name")               //2.2 asignamos el nombre al atributo del objeto person con nombre name
        
        // 3. Realizamos la persistencia en disco. Es necesario usar un try y catch
        do{
            try managedContext.save() //3.1 funcion para guardar o persistir
            people.append(person) //3.2 almacenamos en el array de managed object people

        } catch let error as NSError{ // 3.3 Encaso de erro aqui podemos manejarlos
            print("No hemos podido guardar debido al error \(error), \(error.userInfo)") // userInfo muestra array variables de estado del usuario
        }
    }
    
    //MARK:  - Métodos de UITableViewDataSource Nesecarios para usar UITableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int { // Numero de filas segun el numero de objetos de array people
        return people.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell { // se configura la reutilizacion de celdas se usan 12 o 14 siempre
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")      // Que celda se estara ocupando para mostrar los datos
        let person = people[indexPath.row]                                  // array people tienen objetos de tipo ManageObject ya no es un string que se pueda asignar de manera directa
        cell?.textLabel?.text =  person.valueForKey("name") as? String      // Se asigna el valor del atributo name y se hace un casting a String ya que es un object
        return cell!                                                        // Necesitamos exista la celda, aseguramos que existe usamos !
    }
    
    
   
}

