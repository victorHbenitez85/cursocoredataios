//
//  Dish+CoreDataProperties.swift
//  ElBarDeJB
//
//  Created by Victor Hugo Benitez Bosques on 03/09/16.
//  Copyright © 2016 Juan Gabriel Gomila Salas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Dish {

    @NSManaged var name: String?
    @NSManaged var searchKey: String?
    @NSManaged var imageName: NSData?
    @NSManaged var lastEaten: NSDate?
    @NSManaged var timesEaten: NSNumber?
    @NSManaged var rating: NSNumber?
    @NSManaged var isFavorite: NSNumber?
    @NSManaged var color: NSObject?

}
