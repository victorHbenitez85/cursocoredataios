//
//  ViewController.swift
//  ElBarDeJB
//
//  Created by Juan Gabriel Gomila Salas on 1/6/16.
//  Copyright © 2016 Juan Gabriel Gomila Salas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet var segmentedControl: UISegmentedControl!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelRating: UILabel!
    @IBOutlet var labelEaten: UILabel!
    @IBOutlet var labelLastEaten: UILabel!
    @IBOutlet var labelFavourite: UILabel!
    
    
    //Expand ManagedObjectContext to differents Views
    var managedContext : NSManagedObjectContext!
    var currentDish : Dish! //Get the current Dish
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        insertMainDishes() //1.- Inserta los platillos de la plist a Core data
        
        // 2. Peticion de traer el primer platillo de Core Data
        let fetchRequest = NSFetchRequest(entityName: "Dish")           //Request get Dishes Core Data
        let firstTitle = segmentedControl.titleForSegmentAtIndex(0)     // Get title segmentedControl Index 0
        fetchRequest.predicate = NSPredicate(format: "searchKey == %@", firstTitle!)    // filter title with firstTitle
        
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [Dish] //guardamos los objetos en un array de objetos Dish
            currentDish = results.first!    //Save de index o of array results
            updateViewWithDish(currentDish)  //mostramos el primer objeto Dish
            
        }catch let error as NSError{
            print("\(error.localizedDescription)")
        }
        
    }
    
    
    //3. funcion que muestra en pantalla el objeto de Core data llamado
    func updateViewWithDish(dish : Dish) {
        imageView.image = UIImage(data: dish.imageName!)
        labelName.text = dish.name
        labelRating.text = "Valoracion del platillo \(dish.rating!.doubleValue)/5"
        labelEaten.text = "Veces degustado \(dish.timesEaten!.integerValue)"
        labelFavourite.hidden = !(dish.isFavorite!.boolValue)
        
        //para la fecha usamos un formateador de fechas
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .NoStyle
        labelLastEaten.text = "Ultima vez" + dateFormatter.stringFromDate(dish.lastEaten!)
        
        //cambiamos el color a la vista
        view.tintColor = dish.color as! UIColor
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func insertMainDishes(){ // 1.- Insert dish of type objects
        let fetchRequest = NSFetchRequest(entityName: "Dish") //request to check the dishes were insert before
        fetchRequest.predicate = NSPredicate(format: "searchKey != nil") //condition to check searchKey different of nil
        
        let countDishes = managedContext.countForFetchRequest(fetchRequest, error: nil)
        
        if countDishes > 0 { //si hay plato no inserter ningun otro
            return
        }
        
        let path = NSBundle.mainBundle().pathForResource("SampleData", ofType: "plist") //ruta de la property list
        let dataArray = NSArray(contentsOfFile: path!)! //genera un array de objetos a traves del property list
        
        for foodDict : AnyObject in dataArray{ //dataArray es un anyObject y Dictionary
            //Proceso guardar objeto en el Core Data
            let entity = NSEntityDescription.entityForName("Dish", inManagedObjectContext: managedContext)  //Set entity Dish
            let dish = Dish(entity: entity!, insertIntoManagedObjectContext: managedContext) //Create dish object with constuctor and persist the Data
            
            let theFoodDict = foodDict as! NSDictionary // tenemos que usarlo como array de dictionaries realizamos un casting
            
            //Add Core date the objects
            dish.name = theFoodDict["name"] as? String
            dish.searchKey = theFoodDict["searchKey"] as? String
            dish.rating = theFoodDict["rating"] as? NSNumber
            dish.lastEaten = theFoodDict["lastEaten"] as? NSDate
            dish.timesEaten = theFoodDict["timesEaten"] as? NSNumber
            dish.isFavorite = theFoodDict["isFavorite"] as? NSNumber
            
            // tranformacion del color de dictionaru a UIColor
            let colorDict = theFoodDict["tintColor"] as? NSDictionary   // Se guarda una variable el diccionario de tintColot
            dish.color = colorFromDict(colorDict!)                      // Funcion que regresara un UIColor del dictionary
            
            //Tratamiento de la imagen de String a NSDate : AnyObject
            let imageName = theFoodDict["imageName"] as? String         //Get name image of plist
            let image = UIImage(named: imageName!)                      //create object UIImage with the paramater imageName
            let imageData = UIImagePNGRepresentation(image!)            //transform object UIImage to Bits map
            dish.imageName = imageData                                  // save image data in the Core Data
            
        }
        
    }
    
    
    func colorFromDict(dict : NSDictionary) -> UIColor {
        let red = dict["red"] as! NSNumber
        let green = dict["green"] as! NSNumber
        let blue = dict["blue"] as! NSNumber
        let color = UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
        return color
     
    }
    
    
    @IBAction func foodSelected(sender: UISegmentedControl) {
        //titulo de selected control seleccionado
        let selectedTitle = sender.titleForSegmentAtIndex(sender.selectedSegmentIndex)
        
        //peticion para mostrar el objeto dish con recpecto al titulo del segmentedControl
        let fetchRequest = NSFetchRequest(entityName: "Dish")
        fetchRequest.predicate = NSPredicate(format: "searchKey == %@", selectedTitle!)
        
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [Dish]
            currentDish = results.first!
            updateViewWithDish(currentDish)
        }catch let error as NSError{
        print("\(error.localizedDescription)")
        }
    }
    
    @IBAction func eatPressed(sender: UIButton) {
        //modificamos a nivel de clase y objeto de tipo Dish
        var times = currentDish.timesEaten!.integerValue    //atributo veces dish comido guardado en Core Data
        times += 1                                          //conteo segun presionado el boton
        currentDish.timesEaten = NSNumber(integer: times)   //Actualizamos el timesEaten al contador
        
        //Actalizar a la ultima hora
        currentDish.lastEaten = NSDate()        //modificamos el atributo lastEaten Fecha
        
        //Guardamos los cambios en el objeto actual
        do{
            try managedContext.save()
        }catch let error as NSError{
            print(error.localizedDescription)
        }
        
        //Actualizamo la vista
        updateViewWithDish(currentDish)
        
    }

    @IBAction func ratePressed(sender: UIButton) { //Espera a que un botton sea pulsado
       rate()
        
    }
    
    func rate(){ //funcion Crea alercontroller con dos botones y un textfield
        let alertController = UIAlertController(title: "Valoracion", message: "Escribe una valoracion del platillo \(currentDish.name!) del 0 al 5", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil)   //Boton cancelar
        let saveAction = UIAlertAction(title: "Guardar", style: .Default){(action) in       //Boton guardar, agregado textField
            let textField = alertController.textFields![0] as UITextField                   //Creacion del textField em el alertController
            self.updateRating(textField.text!)                                              //Actualizamos la varolacion de acuerdo el texto del textField
        }
        
        alertController.addTextFieldWithConfigurationHandler{(textfield) in   //Agregamos el texfield al alertController
            textfield.keyboardType = .DecimalPad      //Se mostrara un teclado de tipo numero con decimales
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)  //En esta vista muestra el alertController : presentamos el controler a la vista

    
    }
    
    func updateRating(newRating:String){
        //Actualizamos el objeto
        let numberRate = (newRating as  NSString).doubleValue        //NSString puedes usar el atributo doubleValue
        currentDish.rating = numberRate
        
        //Persistir en Code Data
        do{
            try managedContext.save()   //Guardar en Core data
            updateViewWithDish(currentDish)     //actualizar el objeto actual en la vista
        }catch let error as NSError{
            print(error.localizedDescription)
            //Comprobar los errores del rango 0 a 5 puesto en el atributo de Core Data
            if error.domain == NSCocoaErrorDomain && (error.code == NSValidationNumberTooLargeError || error.code == NSValidationNumberTooSmallError){ //llamar al codigo de error de cocoa
                rate()
            }
        }
        
    }

    
}

